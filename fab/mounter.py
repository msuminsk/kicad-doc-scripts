#!/usr/bin/python3

# Copyright (C) 2017-2024 CERN
# @author Maciej Suminski <maciej.suminski@cern.ch>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pcbnew
import wx
import xml.etree.ElementTree as et
import os.path
from time import strftime, localtime

class generate_mount(pcbnew.ActionPlugin):
    '''
    generate_mount: Script to generate assembly files for CERN designs.
    '''

    # Possible units used for location
    INCH    = 1
    MM      = 2

    # Types of generated assemby documents
    MOUNT_TOP       = 1
    MOUNT_BOTTOM    = 2
    BOM             = 3
    PINCOUNT        = 4

    # PCB sides
    TOP         = 1
    BOTTOM      = 2
    BOTH        = 3

    # Mounted flags
    MOUNTED_YES = 1
    MOUNTED_NO  = 2
    MOUNTED_ALL = 3

    # Package type
    PACKAGE_SMD = 1
    PACKAGE_THT = 2
    PACKAGE_ANY = 3


    def defaults(self):
        self.name = 'Generate assembly files'
        self.category = 'Output'
        self.description = 'Generate assembly files for CERN designs'
        self._units = self.INCH


    def _create_fields(self):
        # mounter script uses data coming from XML file created as an
        # intermediate step by the BOM generator
        xmlFilename = self._pcb.GetFileName().replace('.kicad_pcb', '.xml')

        if not os.path.isfile(xmlFilename):
            raise RuntimeError( "File %s does not exist. Run the BOM exporter first." % xmlFilename )

        xmlTree = et.parse(xmlFilename)
        self._fields = dict()

        for component in xmlTree.getroot()[1]:
            fields = dict()

            try:
                for field in component.find('fields'):
                    fields[field.attrib['name']] = field.text
            except:
                pass # component without fields

            self._fields[component.attrib['ref']] = fields


    def _get_field(self, ref, fieldName):
        try:
            return self._fields[ref][fieldName]
        except:
            return None


    @staticmethod
    def _field_empty(fieldVal):
        if fieldVal is None:
            return True

        # in older KiCad versions empty fields were forbidden,
        # so they were replaced with tildas
        fieldVal = fieldVal.strip()
        return (fieldVal == '') or (fieldVal == '~') or (fieldVal == '~~')


    def _conv_coords(self, value):
        point = wx.Point

        # Y axis is inverted
        if self._units == self.INCH:
            point.x = (value.x - self._auxOrigin.x) / 2540.0
            point.y = (self._auxOrigin.y - value.y) / 2540.0
        elif self._units == self.MM:
            point.x = (value.x - self._auxOrigin.x) / 1000.0
            point.y = (self._auxOrigin.y - value.y) / 1000.0
        else:
            raise ValueError('Invalid output unit')

        return point


    def _get_filename(self, boardName, docType):
        if docType == self.MOUNT_BOTTOM:
            return boardName.replace('.kicad_pcb', '_mounter-bot.asc')
        elif docType == self.MOUNT_TOP:
            return boardName.replace('.kicad_pcb', '_mounter-top.asc')
        elif docType == self.BOM:
            return boardName.replace('.kicad_pcb', '_bom.asc')
        elif docType == self.PINCOUNT:
            return boardName.replace('.kicad_pcb', '_pincount.asc')


    def _get_layer_name(self, layer):
        if layer == self.BOTTOM:
            return 'Bottom'
        elif layer == self.TOP:
            return 'Top'
        else:
            raise ValueError('Invalid layer')


    def _get_layer(self, docType):
        if docType == self.MOUNT_BOTTOM:
            return self.BOTTOM
        if docType == self.MOUNT_TOP:
            return self.TOP
        else:
            return self.BOTH


    def _count_components(self, layer, mounted_flag, package_type):
        count = 0

        for m in self._pcb.GetFootprints():
            if not self._check_component(m, mounted_flag, package_type):
                continue

            if layer == self.BOTTOM and m.IsFlipped():
                count = count + 1
            elif layer == self.TOP and not m.IsFlipped():
                count = count + 1
            elif layer == self.BOTH:
                count = count + 1

        return count


    def _is_fiducial(self, module):
        partNumber = module.GetValue()
        return "FIDUCIAL" in partNumber.upper()


    def _write_mount_line(self, output, m, docType):
        # output components only on the selected side of the board
        if(docType == self.BOTTOM) != m.IsFlipped():
            return

        pos = self._conv_coords(m.GetPosition())
        output.write('%s|%f|%f|%d|%s|%s\n' % (m.GetReference(), pos.x, pos.y,
                    m.GetOrientationDegrees(), m.GetValue(),
                    m.GetFPID().GetLibItemName()))


    def _generate_mount(self, docType):
        outFileName = self._get_filename(self._pcb.GetFileName(), docType)
        layer = self._get_layer(docType)
        component_count = self._count_components(layer, self.MOUNTED_YES, self.PACKAGE_SMD)

        if component_count == 0:
            return

        output = open(outFileName, 'w')
        output.write('%s|\n\n' % outFileName)

        output.write('%s Mounter List - ' % self._get_layer_name(docType))
        output.write('%d SMD Components on %s / %d Total Components - %s\n'
                % (component_count,
                    self._get_layer_name(layer).upper(),
                    self._count_components(self.BOTH, self.MOUNTED_ALL, self.PACKAGE_ANY),
                    strftime('%d/%m/%Y %I:%M:%S %p', localtime())))

        if self._units == self.INCH:
            output.write('Units = INCH\n\n')
        elif self._units == self.MM:
            output.write('Units = MM\n\n')
        else:
            raise ValueError('Invalid output unit')

        output.write('Designator | Mid-X | Mid-Y | Rotation | Part_Number | Footprint\n')

        #C141|3.18936|8.80613|0|CC1206_22UF_10V_10%_X7R|CAPC3216X180N
        # fiducials
        for m in filter(lambda x: self._is_fiducial(x), self._pcb.GetFootprints()):
            self._write_mount_line(output, m, docType)

        # all other *mounted* SMD components
        for m in filter(lambda x : not self._is_fiducial(x) and self._is_mounted(x) and self._is_smd(x),
                self._pcb.GetFootprints()):
            self._write_mount_line(output, m, docType)

        output.close()


    def _is_mounted(self, module):
        ref = module.GetReference()
        mounted_field = self._get_field(ref, 'Mounted')

        if mounted_field:
            return mounted_field.upper() != 'NO'
        else:
            return True     # when 'Mounted' field does not exist, the component should be mounted


    def _get_population(self, module):
        if self._is_fiducial(module):
            return '_'

        if not self._is_mounted(module):
            return 'DNF'  # do not fit

        try:
            ref = module.GetReference()
            fields = self._fields[ref]

            if fields['SMD'].upper() == 'NO':
                return 'THT'
            if fields['SMD'].upper() == 'YES':
                return 'SMD'
        except:
            pass

        return None


    def _is_smd(self, module):
        return self._get_population(module) == 'SMD'


    def _is_tht(self, module):
        return self._get_population(module) == 'THT'


    def _check_component(self, module, mounted_flag, package_type):
        mounted = self._is_mounted(module)
        population = self._get_population(module)

        # filter mounted/not mounted components depending on the mounted_flag
        if mounted_flag == self.MOUNTED_YES and not mounted:
            return False

        if mounted_flag == self.MOUNTED_NO and mounted:
            return False

        # filter by package type
        if package_type == self.PACKAGE_SMD and population != 'SMD':
            return False

        if package_type == self.PACKAGE_THT and population != 'THT':
            return False

        return True


    def _generate_bom(self):
        outFileName = self._get_filename(self._pcb.GetFileName(), self.BOM)
        output = open(outFileName, 'w')

        #:REM - EDA-03538-V1-0_bom.asc - 20/12/2016 8:17:47 AM
        output.write(':REM - %s - %s\n' %
                (outFileName, strftime('%d/%m/%Y %I:%M:%S %p', localtime())))

        output.write(':Reference | Part_number | Package | Outline | PopulationType | Value |\n')

        for m in sorted(self._pcb.GetFootprints(), key=lambda x: x.GetReference()):
            try:
                population = self._get_population(m)

                if population is None:  # fields without population info are not listed in BOM
                    continue

                ref = m.GetReference()
                fields = self._fields[ref]

                outline = self._get_field(ref, 'Case')
                if generate_mount._field_empty(outline):
                    outline = '_'

                part_number = self._get_field(ref, 'Manufacturer Part Number')
                if generate_mount._field_empty(part_number):
                    part_number = '_'
                elif not self._is_mounted(m):
                    # If there are parts where some components are mounted, and some are not,
                    # they need to have different part numbers to distinguish them
                    part_number += '(NM)'

                value = self._get_field(ref, 'Val')
                if generate_mount._field_empty(value):
                    value = '_'

                output.write('%s|%s|%s|%s|%s|%s|\n' %
                        (ref.strip(), part_number.strip(), m.GetFPID().GetLibItemName(),
                            outline.strip(), population.strip(), value.strip()))
            except Exception as e:
                print('{0} has no field information: {1}'.format(ref, str(e)))

        output.close()


    def _count_pins(self, layer, mounted_flag, package_type):
        count = 0

        for m in self._pcb.GetFootprints():
            if not self._check_component(m, mounted_flag, package_type):
                continue

            for pad in m.Pads():
                if pad.IsOnLayer(pcbnew.F_Cu) and pad.IsOnLayer(pcbnew.B_Cu) and layer == self.BOTH:
                    count = count + 1
                elif pad.IsOnLayer(pcbnew.F_Cu) and layer == self.TOP:
                    count = count + 1
                elif pad.IsOnLayer(pcbnew.B_Cu) and layer == self.BOTTOM:
                    count = count + 1

        return count


    def _generate_pincount(self):
        outFileName = self._get_filename(self._pcb.GetFileName(), self.PINCOUNT)
        output = open(outFileName, 'w')

        thtPins = self._count_pins(self.BOTH, self.MOUNTED_YES, self.PACKAGE_THT)
        smdTopPins = self._count_pins(self.TOP, self.MOUNTED_YES, self.PACKAGE_SMD)
        smdBotPins = self._count_pins(self.BOTTOM, self.MOUNTED_YES, self.PACKAGE_SMD)
        totalPins = thtPins + smdTopPins + smdBotPins

        output.write('Through pins:        %d\n' % thtPins)
        output.write('SMD pins on TOP:     %d\n' % smdTopPins)
        output.write('SMD pins on BOTTOM:  %d\n' % smdBotPins)
        output.write('Total nb of pins:    %d\n' % totalPins)

        try:
            dimensions = self._pcb.ComputeBoundingBox(True)
            width = dimensions.GetWidth()
            height = dimensions.GetHeight()

            if width != 0 and height != 0:
                output.write('PCB dimensions:      %0.1f mm x %0.1f mm\n'
                        % (width / 10e5, height / 10e5))
        except:
            pass    # not sure if GetBoardEdgesBoundingBox() is always available

        output.close()


    def _display_message(self, title, text):
        _pcbnew_frame = list(filter(lambda w: w.GetTitle().endswith('PCB Editor'),
                wx.GetTopLevelWindows()))[0]

        if _pcbnew_frame is None:
            return

        dlg = wx.MessageDialog(_pcbnew_frame, text, title, wx.OK)
        dlg.ShowModal()
        dlg.Destroy()


    def Run(self):
        try:
            self._pcb = pcbnew.GetBoard()
            self._auxOrigin = self._pcb.GetDesignSettings().GetAuxOrigin()
            self._create_fields()

            self._generate_mount(self.TOP)
            self._generate_mount(self.BOTTOM)
            self._generate_bom()
            self._generate_pincount()
            self._display_message('Generate assembly files', 'Finished')

        except RuntimeError as e:
            self._display_message('Error', str(e))

generate_mount().register()
