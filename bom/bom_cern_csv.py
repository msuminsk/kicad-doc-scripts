#!/usr/bin/python3

# Copyright (C) 2017-2024 CERN
# @author Maciej Suminski <maciej.suminski@cern.ch>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

"""
    @package
    Generate a csv BOM list (adapted for DEM projects)
    Components are sorted and grouped by request fields.

    Command line:
    python "pathToFile/bom_csv_grouped_by_value.py" "%I" "%O.csv"
"""

# TODO dragging fields to change the order
# TODO file browser for the file selector

from __future__ import print_function
import os
import sys

# workaround for Windows
modpath = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(modpath)
sys.path.insert(0, fullpath)

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import csv
import wx
import codecs
import xml.etree.ElementTree as et

try:
    import configparser
except:
    # Python 2
    import ConfigParser as configparser


def componentCompare(self, other, groupFields):
    """componentCompare is a more advanced equivalence function for components which is
    used by component grouping.
    """
    for field in groupFields:
        if self.getField(field, True) != other.getField(field, True):
            return False

    return True


class DIALOG_BOM(wx.Frame):
    """
    This is DIALOG_BOM.  It just shows a few controls on a wxPanel,
    and has a simple menu.
    """
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, wx.ID_ANY, title, size=(400, 600))

        panel = wx.Panel(self)
        argc = len(sys.argv)

        # file name text control
        if argc == 2:
            fileName = sys.argv[1].replace('.xml', '.csv')
        elif argc == 3:
            fileName = sys.argv[2]

            # assure correct extension
            if fileName[-4:] != '.csv':
                fileName = fileName + '.csv'
        else:
            print('usage: %s <netlist.xml>' % sys.argv[0])
            self.Close()
            return

        self.netlistFilename = sys.argv[1]

        sizerFileName = wx.BoxSizer(wx.HORIZONTAL)
        labelFileName = wx.StaticText(panel, wx.ID_ANY, "Output file:")
        sizerFileName.Add(labelFileName, 0, wx.ALL, 5)
        self.textFileName = wx.TextCtrl(panel, wx.ID_ANY, fileName)
        sizerFileName.Add(self.textFileName, 1, wx.EXPAND | wx.ALL, 5)

        # group fields list
        sizerGroupFields = wx.BoxSizer(wx.VERTICAL)
        labelGroup = wx.StaticText(panel, wx.ID_ANY, "Grouping fields:")
        sizerGroupFields.Add(labelGroup, 0, wx.ALL, 5)
        self.groupFields = wx.CheckListBox(panel, wx.ID_ANY)
        sizerGroupFields.Add(self.groupFields, 1, wx.EXPAND | wx.ALL, 5)
        self.Bind(wx.EVT_LISTBOX_DCLICK,
                lambda evt : self.OnDblClickField(evt, self.groupFields, self.showFields),
                self.groupFields)

        # show fields list
        sizerShowFields = wx.BoxSizer(wx.VERTICAL)
        labelShow = wx.StaticText(panel, wx.ID_ANY, "Show fields:")
        sizerShowFields.Add(labelShow, 0, wx.ALL, 5)
        self.showFields = wx.CheckListBox(panel, wx.ID_ANY)
        sizerShowFields.Add(self.showFields, 1, wx.EXPAND | wx.ALL, 5)
        self.Bind(wx.EVT_LISTBOX_DCLICK,
                lambda evt : self.OnDblClickField(evt, self.showFields, self.groupFields),
                self.showFields)

        # standard buttons
        sizerButton = wx.BoxSizer(wx.HORIZONTAL)
        buttonOk = wx.Button(panel, wx.ID_ANY, "OK")
        sizerButton.Add(buttonOk, 0, wx.ALL, 5)
        self.Bind(wx.EVT_BUTTON, self.OnGenerate, buttonOk)
        buttonCancel = wx.Button(panel, wx.ID_ANY, "Cancel")
        sizerButton.Add(buttonCancel, 0, wx.ALL, 5)
        self.Bind(wx.EVT_BUTTON, self.OnQuit, buttonCancel)
        buttonSaveConfig = wx.Button(panel, wx.ID_ANY, "Save config")
        sizerButton.Add(buttonSaveConfig, 0, wx.ALL, 5)
        self.Bind(wx.EVT_BUTTON, lambda evt : self.saveConfig(), buttonSaveConfig)

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(sizerFileName, 0, wx.EXPAND | wx.ALL, 5)
        mainSizer.Add(sizerGroupFields, 1, wx.EXPAND | wx.ALL, 5)
        mainSizer.Add(sizerShowFields, 1, wx.EXPAND | wx.ALL, 5)
        mainSizer.Add(sizerButton, 0, wx.EXPAND | wx.ALL, 5)
        panel.SetSizer(mainSizer)
        panel.Layout()

        # Generate an instance of a generic netlist, and load the netlist tree from
        # the command line option. If the file doesn't exist, execution will stop
        self.net = kicad_netlist_reader.netlist(self.netlistFilename)

        # subset the components to those wanted in the BOM, controlled
        # by <configure> block in kicad_netlist_reader.py
        self.components = self.net.getInterestingComponents()

        # Prepare the list of fields
        compfields = self.net.gatherComponentFieldUnion(self.components)
        partfields = self.net.gatherLibPartFieldUnion()

        # remove Reference, Value, Datasheet, Footprint they will come from 'columns' below
        partfields -= set(['Reference', 'Value', 'Datasheet', 'Footprint'])
        columnset = compfields | partfields     # union

        # prepend an initial 'hard coded' list and put the enchillada into list 'columns'
        self.origColumns = ['Quantity', 'Reference', 'Value', 'Footprint', 'Datasheet'] + sorted(list(columnset))

        for column in self.origColumns:
            itemId = self.showFields.Append(column)
            self.showFields.Check(itemId)

        self.loadConfig()


    # moves fields from one listbox to another
    def OnDblClickField(self, evt, src, dest):
        not_group = ('Quantity', 'Reference')

        if evt.GetString() in not_group:
            return

        idx = evt.GetInt()
        checked = src.IsChecked(idx)
        src.Delete(idx)
        idx = dest.Append(evt.GetString())
        dest.Check(idx, checked)


    def OnQuit(self, evt):
        self.Close()


    def OnGenerate(self, evt):
        busyCursor = wx.BusyCursor()

        self.generateNetlist()
        self.generateWorksheetFields()

        del busyCursor
        self.Close()


    def getFieldValue(self, field, group):
        if field == 'Quantity':
            return len(group)

        elif field == 'Reference':
            refs = u''

            for component in group:
                if len(refs) > 0:
                    refs += ', '
                refs += str(component.getRef())

            return refs

        elif field == 'Footprint':  # TODO remove in the future
            return self.net.getGroupFootprint(group)

        else:
            return self.net.getGroupField(group, field)


    def saveConfig(self):
        config_path = fullpath + '/bom_cern_csv.cfg'
        config = configparser.RawConfigParser()
        config.add_section('Group')
        config.add_section('Show')

        for idx in range(0, self.groupFields.GetCount()):
            config.set('Group', self.groupFields.GetString(idx), self.groupFields.IsChecked(idx))

        for idx in range(0, self.showFields.GetCount()):
            config.set('Show', self.showFields.GetString(idx), self.showFields.IsChecked(idx))

        try:
            with open(config_path, 'w') as configfile:
                config.write(configfile)
        except:
            print('Could not save the configuration file')

        print('Config saved as {0}'.format(config_path))


    def loadConfig(self):
        try:
            config = configparser.RawConfigParser()
            config.read(fullpath + '/bom_cern_csv.cfg')

            idx = 0
            while idx < self.showFields.GetCount():
                fieldName = self.showFields.GetString(idx)

                if config.has_option('Group', fieldName.lower()):
                    # move to 'Group' listbox
                    self.showFields.Delete(idx)
                    newIdx = self.groupFields.Append(fieldName)
                    self.groupFields.Check(newIdx, config.getboolean('Group', fieldName.lower()))
                    # do not increment idx
                    continue
                elif config.has_option('Show', fieldName.lower()):
                    self.showFields.Check(idx, config.getboolean('Show', fieldName.lower()))
                else:   # new fields
                    self.showFields.Check(idx, False)

                idx = idx + 1


            #for field in config.items('Group'):
            #    idx = self.groupFields.FindString(field[0])

            #    if idx != wx.NOT_FOUND:
            #        # move a field from 'Show' to group
            #        idxShow = self.groupFields.FindString(field[0])

            #        if idxShow != wx.NOT_FOUND:
            #            self.showFields.Delete(idxShow)
            #            idx = self.groupFields.Append(field[0])
            #            self.groupFields.Check(idx, field[1])

            #for field in config.items('Show'):
            #    idx = self.showFields.FindString(field[0])

            #    if idx != wx.NOT_FOUND:
            #        self.showFields.Check(idx, field[1])

        except:
            pass

    def generateNetlist(self):
        f = open(self.textFileName.GetValue(), 'w', encoding='utf-8-sig')
        #f.write(codecs.BOM_UTF8)    # byte order mark for UTF-8

        # get the list of grouping columns
        grouping = []

        for idx in range(0, self.groupFields.GetCount()):
           if(self.groupFields.IsChecked(idx)):
               grouping.append(self.groupFields.GetString(idx))

        # get the list of columns
        columns = []

        for column in self.origColumns:
            idx = self.showFields.FindString(column)

            if idx != wx.NOT_FOUND and self.showFields.IsChecked(idx):
                columns.append(self.showFields.GetString(idx))
                continue

            idx = self.groupFields.FindString(column)

            if idx != wx.NOT_FOUND and self.groupFields.IsChecked(idx):
                columns.append(self.groupFields.GetString(idx))
                continue

        # Override the component equivalence operator - it is important to do this
        # before loading the netlist, otherwise all components will have the original
        # equivalency operator.
        kicad_netlist_reader.comp.__eq__ = lambda a, b : componentCompare(a, b, grouping)

        # Create a new csv writer object to use as the output formatter
        out = csv.writer(f, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)

        # override csv.writer's writerow() to support encoding conversion (initial encoding is utf8):
        def writerow(acsvwriter, columns):
            utf8row = []
            for col in columns:
                text = str(col)

                # digits need to be properly escaped, so Excel does not treat them
                # as integers and e.g. 0603 does not become 603
                if(text.isdigit()):
                    utf8row.append('="' + str(col) + '"')
                else:
                    utf8row.append(str(col))

            acsvwriter.writerow(utf8row)

        # Output a set of rows as a header providing general information
        writerow(out, columns)                   # reuse same columns

        # Get all of the components in groups of matching parts + values
        # (see kicad_netlist_reader.py)
        grouped = self.net.groupComponents(self.components)


        # Output component information organized by group, aka as collated:
        for group in grouped:
            # Skip "Standard (No BOM)" components
            skip = False

            for field in ('Component Kind', 'Component Type'):
                if self.getFieldValue(field, group) == 'Standard (No BOM)':
                    print('Skipping {0} (No BOM)'.format(self.getFieldValue('Reference', group)))
                    skip = True
                    break

            if skip:
                continue


            row = []

            for field in columns:
                row.append(self.getFieldValue(field, group))

            writerow(out, row)

        f.close()


    def generateWorksheetFields(self):
        xmlTree = et.parse(self.netlistFilename)
        design = xmlTree.getroot().find('design')

        outFilename = self.textFileName.GetValue()
        outFilename = os.path.dirname(outFilename) + '/fields_' + os.path.basename(outFilename)
        f = open(os.path.normpath(outFilename), 'w', encoding='utf-8-sig')
        #f.write(codecs.BOM_UTF8)    # byte order mark for UTF-8

        # for entry in ('source', 'date', 'tool'):
            # try:
                # field = design.find(entry)
                # f.write('%s,%s\r\n' %
                        # (str(field.tag).encode('utf-8'), str(field.text).encode('utf-8')))
            # except:
                # pass

        title_block = design.find('sheet').find('title_block')

        for field in title_block:      # sheet->title_block
            if field.tag == 'comment':
                name = 'comment' + field.attrib['number']
                value = field.attrib['value']
            else:
                name = field.tag
                value = field.text

            if value is None or name is None:
                continue

            f.write('%s,%s\n' % (name, '"' + value + '"'))

        f.close()


class BOM_GENERATOR(wx.App):
    def OnInit(self):
        dialog = DIALOG_BOM(None, "BOM Generator")
        self.SetTopWindow(dialog)

        dialog.Show(True)
        return True


app = BOM_GENERATOR(redirect=False)
app.MainLoop()
